# App Dev: [Setting up a Development Environment][1] - Python

## Create a Compute Engine Virtual Machine Instance

### Install software on the VM instance

```sh
sudo apt-get update
sudo apt-get install git
# Install Python
sudo apt-get install python3-setuptools python3-dev build-essential
# Install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python3 get-pip.py
```

## Configure the VM to Run Application Software

```sh
python3 --version
pip3 --version
git clone https://github.com/GoogleCloudPlatform/training-data-analyst
cd ~/training-data-analyst/courses/developingapps/python/devenv/
sudo python3 server.py
```

Apaga el servidor, listaremos las instancias de GCE.

```sh
sudo pip3 install -r requirements.txt
python3 list-gce-instances.py <PROJECT_ID> --zone=<YOUR_VM_ZONE>
```

[1]: https://www.qwiklabs.com/focuses/1074?parent=catalog